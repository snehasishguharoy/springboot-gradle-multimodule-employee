package com.sirius.xm.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "EmployeeResponse")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmpRes implements Serializable {

	private static final long serialVersionUID = -1664814281453251944L;
	private String message;

}

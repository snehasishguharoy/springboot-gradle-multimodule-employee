package com.sirius.xm.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Employee")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Emp implements Serializable {

	private static final long serialVersionUID = -1716608281003209765L;
	private Integer empId;
	private String name;
	private String address;
	private String emailId;
	private Boolean isActive;

}

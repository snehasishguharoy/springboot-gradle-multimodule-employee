package com.sirius.xm.helper;

import com.sirius.xm.domain.Emp;
import com.sirius.xm.entity.Employee;

public class EmpHelper {

	public static Emp convertToEmp(Employee employee) {
		return new Emp(employee.getEmpId(), employee.getName(), employee.getAddress(), employee.getEmailId(),
				employee.getIsActive());
	}

	public static Employee convertToEmployee(Emp employee) {
		System.out.println(employee.getEmpId());
		return new Employee(employee.getEmpId(), employee.getName(), employee.getAddress(), employee.getEmailId(),
				employee.getIsActive());
	}

}

package com.sirius.xm.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sirius.xm.contract.EmployeeService;
import com.sirius.xm.domain.Emp;
import com.sirius.xm.domain.EmpRes;
import com.sirius.xm.entity.Employee;
import com.sirius.xm.helper.EmpHelper;
import com.sirius.xm.repo.EmpRepo;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmpRepo empRepo;

	@Override
	@Transactional
	public EmpRes saveEmp(Emp employee) {
		System.out.println("In the save method");
		String msg = null;
		if (null != employee) {
			Employee response = empRepo.save(EmpHelper.convertToEmployee(employee));
			if (null == response) {
				msg = "Employee is not inserted successfully";
				return new EmpRes(msg);
			} else {
				msg = "Employee is  inserted successfully";
				return new EmpRes(msg);
			}
		}
		return null;
	}

	@Override
	@Transactional
	public EmpRes updateEmp(Emp employee) {
		System.out.println("In the update method");
		String msg = null;
		Employee emp = empRepo.findById(employee.getEmpId()).get();
		if (null != employee) {
			emp.setEmpId(employee.getEmpId());
			emp.setAddress(employee.getAddress());
			emp.setEmailId(employee.getEmailId());
			emp.setIsActive(employee.getIsActive());
			emp.setName(employee.getName());
			Employee response = empRepo.save(emp);
			if (null == response) {
				msg = "Employee is not updated successfully";
				return new EmpRes(msg);
			} else {
				msg = "Employee is  updated successfully";
				return new EmpRes(msg);
			}
		}
		return null;
	}

	@Override
	public List<Emp> fetchEmps() {
		return empRepo.findAll().stream().map(EmpHelper::convertToEmp).collect(Collectors.toList());
	}

	@Override
	public Emp fetchEmpById(Integer employeeId) {
		return EmpHelper.convertToEmp(empRepo.findById(employeeId).get());

	}

	@Override
	@Transactional
	public EmpRes delEmp(Integer empId) {
		String msg = null;
		Employee emp =empRepo.findById(empId).get();
		emp.setIsActive(false);
		Employee response = empRepo.save(emp);
		if (null == response) {
			msg = "Employee is not deleted successfully";
			return new EmpRes(msg);
		} else {
			msg = "Employee is  deleted successfully";
			return new EmpRes(msg);
		}
	}

}

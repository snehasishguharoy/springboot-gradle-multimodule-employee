package com.sirius.xm.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.sirius.xm.*" })
@EntityScan(basePackages = { "com.sirius.xm.*" })
@EnableJpaRepositories(basePackages = { "com.sirius.xm.*" })
@ImportResource({ "classpath:webservice-definition-beans.xml" })
public class EmpMain {

	public static void main(String[] args) {
		SpringApplication.run(EmpMain.class, args);
	}


	
	
}

package com.sirius.xm.cnfg;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.feature.LoggingFeature;
//import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletPath;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.sirius.xm.service.EmployeeServiceImpl;

@Configuration
public class Cnfg {

	@Bean
	public ServletRegistrationBean dispatcherServlet() {
		return new ServletRegistrationBean(new CXFServlet(), "/services/*");
	}

	@Bean
	public Endpoint endpoint(ApplicationContext applicationContext) {
		EndpointImpl endpoint = new EndpointImpl(springBus(), new EmployeeServiceImpl());
		endpoint.getFeatures().add(new LoggingFeature());
		endpoint.publish("/EmployeeService");
		return endpoint;
	}

	@Bean(name = Bus.DEFAULT_BUS_ID)
	public static SpringBus springBus() {
		SpringBus springBus = new SpringBus();
		return springBus;
	}

	@Bean
	@Primary
	public DispatcherServletPath dispatcherServletPathProvider() {
		return () -> "";
	}

}

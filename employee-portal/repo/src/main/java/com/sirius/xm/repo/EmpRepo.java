package com.sirius.xm.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sirius.xm.entity.Employee;

public interface EmpRepo extends JpaRepository<Employee, Integer> {

}

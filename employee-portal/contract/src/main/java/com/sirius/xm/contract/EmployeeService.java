package com.sirius.xm.contract;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.sirius.xm.domain.Emp;
import com.sirius.xm.domain.EmpRes;

@WebService(serviceName = "EmployeeService")
public interface EmployeeService {

	@WebMethod()
	@WebResult(name = "EmpInsertRes")
	public EmpRes saveEmp(@WebParam(name = "emp") Emp emp);

	@WebMethod()
	@WebResult(name = "EmpUpdateRes")
	public EmpRes updateEmp(@WebParam(name = "employee") Emp employee);

	@WebMethod()
	@WebResult(name = "EmpDelRes")
	public EmpRes delEmp(@WebParam(name = "empId") Integer empId);

	@WebMethod()
	@WebResult(name = "EmpFetchRes")
	public List<Emp> fetchEmps();

	@WebMethod()
	@WebResult(name = "EmpFetchByIdRes")
	public Emp fetchEmpById(@WebParam(name = "employeeId") Integer employeeId);

}
